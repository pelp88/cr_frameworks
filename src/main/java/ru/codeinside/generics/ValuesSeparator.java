package ru.codeinside.generics;

import java.util.ArrayList;
import java.util.List;

public class ValuesSeparator {
    private List<Object> list;

    public void addVariables(List<Object> list) {
        this.list = list;
    }

    public List<Object> getSeparatedBy(Class<?> type) {
        List<Object> output = new ArrayList<>();

        for (Object elem: this.list) {
            if (elem.getClass().getName().equals(type.getName())) {
                output.add(elem);
            }
        }

        return output;
    }
}
